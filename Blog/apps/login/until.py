import re


def login_action(request):
    # login authentication
    if request.user.is_authenticated:
        return {'username': request.user.username, 'id': request.user.id}
    # back to login page
    else:
        return None

def norm_text(text):
    text = re.sub(r'\s+', ' ', text).strip()
    return text

