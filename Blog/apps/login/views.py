from django.shortcuts import render, HttpResponse, redirect
from django.views import View
from django.contrib.auth import authenticate, login, logout
# from django.contrib.auth.decorators import login_required
from .until import login_action

# Create your views here.
class LoginClass(View):
    def get(self, request):
        return render(request, 'homepage/login/Login.html')
    def post(self, request):
        # get username and password when they enter username and password
        username = request.POST.get('user_name') #  get 'user_name' in name feature of input tag
        password = request.POST.get('pass')
        # authenticate user, check the account already exists or not
        my_user = authenticate(username= username, password= password)
        #  if the account is not already exists
        if my_user is None:
            return HttpResponse('the account is not already exist!')
        login(request, my_user)
        return redirect('article:index')

class LogoutClass(View):
    def get(self, request):
        logout(request)
        #  category list
        # categories = Category.objects.all()
        # context = {'categories': categories}
        # user = login_action(request)
        # if user is not None:
        #     context['user'] = user
        return redirect('article:index')
