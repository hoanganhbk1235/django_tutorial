from django.urls import path, include
from .views import LoginClass, LogoutClass

app_name = 'login'
urlpatterns = [
    path('', LoginClass.as_view(), name='login-acc'),
    path('logout/', LogoutClass.as_view(), name='logout-acc'),
]