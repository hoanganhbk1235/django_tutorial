from django.shortcuts import render, HttpResponse, redirect
from django.views import View
from django.contrib import messages
from .forms import NewUserForm, UserProfileForm
from .models import UserProf
from apps.article.models import Category, Article
from apps.login.until import norm_text
import pdb


# Create your views here.

class Registration_view(View):
    def get(self, request):
        form = NewUserForm()
        return render(request, 'homepage/user/registration.html', {'f': form})
    def post(self, request):
        form = NewUserForm(request.POST)
        # print(form)
        if form.is_valid():
            form.save()
            # u = form.username
            username = form.cleaned_data.get('username')
            messages.success(request, f'Your account has been created! You are now able to log in')
            return redirect('login:login-acc')
        return HttpResponse("can't create new account!")

class UserProfile(View):
    def get(self, request, id_user):
        user = UserProf.objects.get(pk=id_user)
        ls_arts = Article.objects.filter(id_user=user)
        categories = Category.objects.all()
        form = UserProfileForm(initial={
            'username':user.username,
            'email': user.email,
            'first_name': user.first_name,
            'last_name': user.last_name,
            'age': user.age,
            'phone': user.phone,
            'sex': user.sex
        })
        context = {'form':form, 'cates':categories, 'articles': ls_arts}
        if (request.user.username == user.username) | (request.user.has_perm('user.change_userprof')):
            sex_choice = []
            for sex in ((0, "Female"), (1, 'Male'), (3, 'None')):
                if sex[0] == user.sex:
                    context['user_sex'] = {'idx': sex[0], 'type': sex[1]}
                sex_choice.append({'idx': sex[0], 'type': sex[1]})
            context['sex_choice'] = sex_choice
            return render(request, 'homepage/user/UserProfile.html', context)
        return render(request, 'homepage/user/UserProfile_for_Customer.html', context)
    def post(self, request, id_user):
        user = UserProf.objects.get(pk=id_user)
        user.first_name = norm_text(request.POST['first_name'])
        user.last_name = norm_text(request.POST['last_name'])
        user.phone = str(request.POST['phone']).strip()
        user.age = int(request.POST['age'])
        user.sex = int(request.POST['sex'])
        user.save()
        return redirect('user:profile', id_user=id_user)


class ChangePassword(View):
    def get(self, request, id_user):
        user = UserProf.objects.get(pk=id_user)
        if request.user.username == user.username:
            return render(request, 'homepage/user/ChangePassword.html', {'user': user})
        return redirect('user:profile', id_user=id_user)
    def post(self, request, id_user):
        user = UserProf.objects.get(pk=id_user)
        cur_pass = request.POST['current-pass']
        new_pass1 = request.POST['new-pass1']
        new_pass2 = request.POST['new-pass2']
        if user.check_password(cur_pass):  # check current password
            if new_pass1 == new_pass2:
                user.set_password(new_pass1)
                user.save()  # update new password
                return redirect('user:profile', id_user=id_user)
        return render(request, 'homepage/error/Error_pass.html', {'id_user':id_user})