from django.urls import path, include
from django.contrib.auth.decorators import login_required
from .views import  Registration_view, UserProfile, ChangePassword
app_name = 'user'
urlpatterns = [
    path('register/', Registration_view.as_view(), name='register'),
    path('<int:id_user>', login_required(UserProfile.as_view(), login_url='/login/'), name='profile'),
    path('change-password/<int:id_user>', login_required(ChangePassword.as_view(), login_url='/login/'), name='change-pass'),
]