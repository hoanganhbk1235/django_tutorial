# Generated by Django 3.2.5 on 2021-08-01 14:58

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userprof',
            old_name='address',
            new_name='phone',
        ),
    ]
