from django.db import models
from django.contrib.auth.models import AbstractUser

# Create your models here.


# we use AbstractUser is base class to be applied by Django, it contain the base features of user
class UserProf(AbstractUser):
    sex_choice = ((0, "Female"), (1, 'Male'), (3, 'None'))
    age = models.IntegerField(default=0)
    sex = models.IntegerField(choices= sex_choice, default= 0)
    phone = models.CharField(default= '', max_length= 255)
    # avatar_img = models.ImageField(upload_to='')

