from django import forms
from django.contrib.auth.forms import UserCreationForm
from .models import UserProf

class NewUserForm(UserCreationForm):
    email = forms.EmailField(max_length=60, help_text="Required. Add a valid email address", required=True)
    class Meta:
        model = UserProf
        # UserProf is create User model of django,
        # UserProf will contain the fields: username,
        fields = ("username", "email", "password1", "password2", 'sex', 'age')
        widgets = {
            'username': forms.TextInput(attrs={'class': 'username_re', 'id': 'username_re'}),
            'email': forms.EmailInput(attrs={'class': 'email_re'}),
            'password1': forms.PasswordInput(attrs={'class': 'password-1'}),
            'password2': forms.PasswordInput(attrs={'class':'password-2'}),
            'age': forms.NumberInput(attrs={'class': 'age', 'id': 'age'})
        }


class UserProfileForm(UserCreationForm):
    email = forms.EmailField(max_length=60, help_text="Required. Add a valid email address", required=True)
    class Meta:
        model = UserProf
        # UserProf is create User model of django,
        # UserProf will contain the fields: username,
        fields = ("username", "email", "first_name", "last_name", 'age', 'phone', 'sex')

