from django.db import models
from django.utils import timezone
from apps.user.models import UserProf

# Create your models here.

class Category(models.Model):
    #  null=False and blank=False, this field is not allowed to be null and blank (default= False)
    name_cate = models.CharField(max_length=100, null=False, blank=False)
    def __str__(self):
        return self.name_cate

class SubCategory(models.Model):
    #  name of sub-cate
    name_cate = models.CharField(max_length=100)
    def __str__(self):
        return self.name_cate

class Article(models.Model):
    title = models.CharField(max_length=512)
    content = models.TextField(null=False, blank=False)
    published = models.DateField(default= timezone.datetime.now)
    #  on_delete=models.DO_NOTHING is as like on_delete=models.CASCADE
    # models.DO_NOTHING: we can't remove parents object, when children object does exit
    # models.CASCADE: when we remove parents object, children object will be removed
    id_cate = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True, default=None)
    id_sub_cate = models.ForeignKey(SubCategory, on_delete=models.CASCADE, null=True, blank=True, default=None)
    id_user = models.ForeignKey(UserProf, on_delete=models.CASCADE, null=True, blank=True, default=None)

    def __str__(self):
        return self.title

class Comment(models.Model):
    id_article = models.ForeignKey(Article, on_delete=models.CASCADE)
    id_user = models.ForeignKey(UserProf, on_delete=models.CASCADE, default=None)
    content = models.CharField(max_length=1000)
    published = models.DateField(default=timezone.datetime.now)
    vote = models.IntegerField(default=0)

    def __str__(self):
        return self.content

class SubComment(models.Model):
    id_comment = models.ForeignKey(Comment, on_delete=models.CASCADE)
    content = models.CharField(max_length=1000)
    published = models.DateField(default=timezone.datetime.now)
    def __str__(self):
        return self.content