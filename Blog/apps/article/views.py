import pdb
from django.shortcuts import render, HttpResponse, redirect
from django.views import View
from .models import Article, Category, SubCategory, Comment
from .forms import AddArticleForm, CommentForm, UpdateArticleForm
from apps.user.models import UserProf
from apps.login.until import login_action, norm_text


# Create your views here.


#  category list
categories = Category.objects.all()


class IndexView(View):
    def get(self, request):
        #  get first 5 artilces in database
        ls_article = Article.objects.all()[:5]
        context = {'articles': ls_article, 'categories': categories}
        #  check login status of client
        user = login_action(request)
        context['user'] = user
        # print('login action: ', context)
        return render(request, 'homepage/index.html', context)
    def post(self, request):
        query_value = norm_text(request.POST['search_value'])
        if query_value != '':
            # pdb.set_trace()
            result = Article.objects.filter(title__icontains=query_value)
            context = {'articles': result, 'categories': categories, 'user': login_action(request)}
            return render(request, 'homepage/article/search_result.html', context)
        else:
            return redirect('article:index')


class CateViewList(View):
    def get(self, request, id_cate):
        cate = Category.objects.get(pk=id_cate)
        #  get first 5 artilces in database
        article = Article.objects.filter(id_cate=cate)[:5]
        context = {'articles': article}

        context['categories'] = categories
        #  check login status of client
        user = login_action(request)
        context['user'] = user
        return render(request, 'homepage/article/cates_list.html', context)


class ArticleView(View):
    def get(self, request, id_article):
        #  get first 5 artilces in database
        article = Article.objects.get(pk=id_article)
        ls_comments = Comment.objects.filter(id_article=id_article)
        initial = { 'id_article': article }
        if request.user.is_authenticated: # check login
            initial['id_user'] =  UserProf.objects.get(pk=request.user.id)
        else:
            initial['id_user'] = None
        comt_form = CommentForm(initial= initial)
        context = {'article': article, 'categories': categories, 'user': login_action(request), 'cmts':ls_comments, 'cmt_f':comt_form}

        # print the whole permisstions of user:
        # print(request.user.get_all_permissions())

        #  check if user is author or super-user, user can update or delete article
        if (request.user.username == article.id_user.username) | (request.user.has_perm('article.change_article')):
            context['update'] = 1
        if (request.user.username == article.id_user.username) | (request.user.has_perm('article.delete_article')):
            context['delete'] = 1
        return render(request, 'homepage/article/article_detail.html', context)
    def post(self, request, id_article):
        if request.user.is_authenticated:  # check login
            pdb.set_trace()
            cmt = CommentForm(request.POST)
            if cmt.is_valid():
                cmt.save()
                return redirect('article:article_detail', id_article=id_article)
            return render(request, 'homepage/error/Error_cmt.html', {'id_article':id_article})
        else:
            return redirect('login:login-acc')


class AddNewArticle(View):
    def get(self, request):
        initial ={
            'id_user': UserProf.objects.get(pk=request.user.id)
        }
        form = AddArticleForm(initial=initial)
        context = {'form': form}
        return render(request, 'homepage/article/add_article.html', context)

    def post(self, request):
        data = AddArticleForm(request.POST)
        #  get data of the each input tag during name attributes to save a varibale
        if data.is_valid():
            #  save the data object to db
            data.save()
            article = Article.objects.get(title=data.data['title'])
            return redirect('article:article_detail', id_article=article.id)
        return render(request, 'homepage/error/Error_add.html')

    def put(self, request):
        pass

class UpdateArticle(View):
    def get(self, request, id_article):
        article = Article.objects.get(pk=id_article)
        #  check if user is author or super-user, user can update or delete article
        if (request.user.username == article.id_user.username) | (request.user.has_perm('article.change_article')):
            form = UpdateArticleForm(initial={
                'title': article.title,
                'content': article.content,
                'id_cate': article.id_cate,
                'id_sub_cate': article.id_sub_cate
            })
            context = {'article': article, 'form': form, 'cates':categories, 'sub_cates': SubCategory.objects.all()}
            return render(request, 'homepage/article/update_article.html', context)
        else:
            return render(request, 'homepage/error/Error_permission.html', {'id_article': id_article})
    def post(self, request, id_article):
        article = Article.objects.get(pk=id_article)
        article.title = norm_text(request.POST['title'])
        article.content = request.POST['content']
        if  article.id_cate.id != int(request.POST['id_cate']):
            article.id_cate = Category.objects.get(pk=request.POST['id_cate'])
        if article.id_sub_cate.id != int(request.POST['id_sub_cate']):
            article.id_sub_cate = SubCategory.objects.get(pk=request.POST['id_sub_cate'])
        article.save(force_update=True)
        return redirect('article:article_detail', id_article=id_article)

class DeleteArticle(View):
    def get(self, request, id_article):
        article = Article.objects.get(pk=id_article)
        if (request.user.username == article.id_user.username) | (request.user.has_perm('article.delete_article')):
            return render(request, 'homepage/article/delete_article.html', {'article': article})
        else:
            return render(request, 'homepage/error/Error_permission.html', {'id_article': id_article})
    def post(self, request, id_article):
        article = Article.objects.get(pk=id_article)
        if request.POST['delete'] == 'Yes':
            article.delete()
            return redirect('article:index')
        return redirect('article:article_detail', id_article=id_article)


