from django.urls import path, include
from django.contrib.auth.decorators import login_required
from .views import IndexView, CateViewList, ArticleView, AddNewArticle, UpdateArticle, DeleteArticle

app_name = 'article'
urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('search/', IndexView.as_view(), name='search-result'),
    path('cate/<int:id_cate>', CateViewList.as_view(), name='cate-list'),
    path('detail/<int:id_article>', ArticleView.as_view(), name='article_detail'),
    path('update/<int:id_article>', UpdateArticle.as_view(), name='update_article'),
    path('delete/<int:id_article>', DeleteArticle.as_view(), name='delete_article'),
    path('new/', login_required(AddNewArticle.as_view(), login_url='/login/'), name='add-new'),
]