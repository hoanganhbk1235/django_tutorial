1. create venv environment
    + python3 -m venv venv_name(ex: venv_ha)
    + source venv_ha/bin/activate

    + if setup mysql in project, we need install mysqlclient in my computer
        + sudo apt-get install libmysqlclient-dev

2. install library
    + in this project we use django==3.1.4

    + pip install -r requirements.txt

        - pip install mysqlclient
        - pip install django

    + or if we install a library
        - pip install lib_name
        - pip freeze > requirements.txt (update the lib infor into requirements.txt file)

3.0: project structure
    + Database
        + users: contain the user info
        + articles: contain aticle is created by users
        + comments: cotain the action between user and articles
            - in the next version: we create sub-comment make to reply comment
    + HahaBlog
        - users app:
            - manage(add, update, remove) user info and their articles
        - articles app:
            - index (main project):
                + task bar: register, login, logout
                + content: show the articles of users and create news article
            -  manage articles and comment
        - login

    + List sites:
        + home (done): show list article, (require log in) create new article
            + search (1/1)
        + article_detail (4/5): show article detail, (require log in) comment, update, remove
                (update, remove: username login == author or username login must have updating and removing permistion)
                -  update article detail (1/1): update and edit post
                -  remove article detail (1/1): remove post
                - comment: (in the next version)(1/5) show, like, reply, update, remnove comment
        + add_article (done): (require log in) add new article
        + login (2/3): log in, log out, forgot password
            - password reset (0/2): when user forgot password, authenticate during gmail
                    (https://dev.to/merichard123/django-diaries-password-reset-email-3d78)
                    (https://github.com/CoreyMSchafer/code_snippets/tree/master/Django_Blog/12-Password-Reset)
        + register (done): register new account
        + user_profile(done): (require log in) show user info, update, change password (remove in admin)
            - change password (1/1): change new password
            - update user info (1/1): update and edit user infor
        + site_cate(Python, English, ...) (done): show list article follow category


3. project
    + create project: django-admin startproject project_name(ex: HahaBlog) .
    + run project:
        - cd ./api_restFramework
        - python manage.py runserver
        - or if we want to change port: python manage.py runserver 8080
    
    + python manage.py: result is the choices

4. create apps folder to contain the apps of project
    + django-admin startapp app_name(ex: user, article, login)
    + create learn and user app : ./Blog/apps/user (article, login)
    + ./Blog/apps/users (articles, login)/apps.py: name = 'apps.user' ('apps.article', ...)
    + register the apps into setting file: ./Blog/HahaBlog/settings.py
        - INSTALLED_APPS = [
            ...,
            'apps.article',
            'apps.user',
        ]
    + config apps url with project : ./Blog/HahaBlog/urls.py
        urlpatterns = [
            ...,
            path('learn/', include('apps.article.urls')),
            path('learn/', include('apps.user.urls')),
        ]
    + create the models in learn and user apps
        # when we create UserProf model in user app, we must authen user model
        #  because user model is default model of django, we just add more info of user
        - ./Blog/HahaBlog/settings.py:  AUTH_USER_MODEL= 'app_name.UserProf' ('user.UserProf')

        + test: python manage.py shell
            -> from apps.article.models import Article
            -> obj = Article.objects.get(pk=1)
            -> print(obj.content)
            -> obj.content = 'asdf'
            -> obj.save(force_update=True)  # update

    + create forms data from models: ./Blog/apps/article (user, login, ...)/forms.py
        + form use model: Articles, Comment, ..
        + form don't use model:

    + register the models with admin in the app:
        - ./Blog/apps/user/admin.py
        - the same for another models
    + create default database
        - python manage.py makemigrations
        - python manage.py migrate

    + create superuser: 
        - python manage.py createsuperuser
            (haha123 - ha123456789)

5. get data from database from models and to show on website
    + test : python manage.py shell

    + create templates and static
        + ./final_project/templates (static)
        + register templates with project: ./Blog/HahaBlog/settings.py
            + TEMPLATES = [
                {
                    ...,
                    'DIRS': [os.path.join(BASE_DIR, "templates")],
                    ...,
                    'OPTIONS': {
                        'context_processors': [
                            ...,
                        ], }, }, ]
            + STATICFILES_DIRS = [
                        os.path.join(BASE_DIR, "static"),
                    ]


